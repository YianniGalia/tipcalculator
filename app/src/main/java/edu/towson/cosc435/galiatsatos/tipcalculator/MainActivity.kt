package edu.towson.cosc435.galiatsatos.tipcalculator

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.RadioGroup
import android.widget.TextView

class MainActivity : AppCompatActivity() {
    private var calcButton: Button? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        calcButton = findViewById(R.id.calcButton)
        calcButton?.setOnClickListener{ handleClick() }
    }
    private fun handleClick() {
        val total_text = findViewById<EditText>(R.id.total_text)

        val inputTotal: String = total_text.editableText.toString()

        val radioGroup = findViewById<RadioGroup>(R.id.radioGroup)

        val tip: Double = when (radioGroup.checkedRadioButtonId) {
            R.id.tip10 -> 0.10
            R.id.tip20 -> 0.20
            R.id.tip30 -> 0.30
            else -> 0.00
        }
        val inputDouble = inputTotal.toDouble()

        val result = convertTip(inputDouble, tip)
        val result_tv = findViewById<TextView>(R.id.result_tv)
        result_tv.text = "Your calculated tip is: $"+result.toString()+" and Your total is: $"+ (result + inputDouble).toString()



    }

    private fun convertTip(input: Double, tip: Double): Double {
        if (tip != 0.0) {
            val totalTip=input * tip
            return totalTip
        }
        else {
            return input * 0.0
        }

    }
}